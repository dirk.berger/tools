#!/bin/bash
# ENABLE EXECUTION
# chmod +x ~/scripts/install-env.sh

# SYSTEM UPDATE
sudo apt update -y
sudo apt upgrade -y

# WGET
sudo apt install wget -y

# CURL
sudo apt install curl -y

# SNAP
sudo apt install snapd -y

#XCLIP
sudo apt install xclip -y

# NVM
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
#sudo apt-get install build-essential libssl-dev
#curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh -o install_nvm.sh

# GVM
sudo snap install gvm

# DOCKER
sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt update -y
sudo apt install docker-ce docker-ce-cli containerd.io -y

# DOCKER-USER
sudo usermod -aG docker $(whoami)

# DOCKER-COMPOSE
sudo curl -L "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# GIT
sudo apt install git -y

# Google Chrome
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | sudo tee /etc/apt/sources.list.d/google-chrome.list
sudo apt update -y 
sudo apt install google-chrome-stable -y

# VISUAL STUDIO CODE 
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
sudo apt update -y
sudo apt install code -y

# POSTMAN
sudo snap install postman

# SQLite3 Database
sudo apt install sqlite3 -y

# SQLite Database Browser
sudo apt install sqlitebrowser -y

# Draw.io
sudo snap install drawio

# SYSTEM UPDATE
sudo apt update -y
sudo apt upgrade -y

wget -O ~/.bash_aliases https://gitlab.com/dirk.berger/tools/-/raw/master/configs/.bash_aliases 
wget -O ~/.bashrc https://gitlab.com/dirk.berger/tools/-/raw/master/configs/.bashrc 
wget -O ~/.profile https://gitlab.com/dirk.berger/tools/-/raw/master/configs/.profile

# REGISTER CHANGES
source ~/.bashrc
source ~/.profile

# NODE + NPM + NPX (GLOBAL)
nvm install stable

# YARN
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update -y
sudo apt install yarn -y

source ~/.bashrc

# ANGULAR - CLI - (GLOBAL)
# npm install -g @angular/cli
yarn global add @angular/cli

source ~/.bashrc

# FLAMESHOT FOR SCREENSHOTS
sudo apt install flameshot

# DIALOG
sudo apt install dialog

# GITUSER
wget -O ~/gituser.sh https://gitlab.com/dirk.berger/tools/-/raw/master/helper/gituser.sh
sudo chmod +x ~/gituser
sudo ln -s ~/gituser.sh /usr/local/bin/gituser

# CREATE DEV-DIRECTORY
cd ~
mkdir dev

# CHECK
echo "========== WGET =========="
wget --version
echo "========== CURL =========="
curl --version
echo "========== SNAP =========="
snap --version
echo "========== XCLIP =========="
xclip -version
echo "========== NVM =========="
nvm --version
echo "========== GVM =========="
gvm --version
echo "========== DOCKER =========="
docker --version
echo "========== DOCKER-COMPOSE =========="
docker-compose --version
echo "========== GIT =========="
git --version
echo "========== VISUAL STUDIO CODE =========="
code --version
echo "========== POSTMAN =========="
which postman
echo "========== SQLITE3 =========="
sqlite3 --version
echo "========== SQLITE3 BROWSER =========="
sqlitebrowser --version
echo "========== NODE =========="
node --version
echo "========== NPM =========="
npm --version
echo "========== NPX =========="
npx --version
echo "========== YARN =========="
yarn --version
echo "========== ANGULAR/CLI =========="
ng --version


echo '-----------------'
echo 'RESTART TERMINAL!'
echo '-----------------'
echo 'CREATE NX-WORKSPACE AFTER RESTRART'
echo 'npx create-nx-workspace'
