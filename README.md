# Tools

## Bionic Beaver Environment
```bash
sudo wget -qO- https://gitlab.com/dirk.berger/tools/-/raw/master/scripts/bionic-environment.sh | bash
```

## Focal Fossa Environment
```bash
sudo wget -qO- https://gitlab.com/dirk.berger/tools/-/raw/master/scripts/focal-environment.sh | bash
```
