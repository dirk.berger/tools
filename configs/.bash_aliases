# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alFh'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

alias gitbranch="git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all"
alias dockermounts="docker container ps --format 'table {{.ID}}\t{{.Names}}\t{{.Image}}\t{{.Mounts}}'"
alias update='sudo apt-get update'
alias upgrade='sudo apt-get upgrade'
alias netzinfo='ifconfig'
alias compinfo='sudo lshw'
alias version=' echo ----------------nodejs : && nodejs --version &&
                echo ----------------curl : && curl --version &&
                echo ----------------npm : && npm --version
                echo ----------------docker : && docker --version
                echo ----------------java : && java -version'
                
# Shortcuts
alias depe='dep ensure'
alias dcom='docker-compose'
alias d='docker'
alias g='git'
alias mk='mkdir'
alias t='touch'
alias dp='docker ps -a'
alias sb='source ~/.bashrc'
alias ws='yarn workspace-schematic'

alias gitconf='git config user.email "post@dirkberger.com" && git config user.name "Dirk Berger"' 
