#!/bin/bash


cmd=(dialog --separate-output --checklist "Set gitconfig user.name and user.email (no password):" 22 76 16)
options=(1 "gitlab.com (post@dirkberger.com)" off    # any option can be set to default to "on"
         2 "git.highrn.de (dirk.berger@dirkberger.com)" off
         3 "git.sp-con.de (dirk.berger@sp-con.de)" off
         4 "remove .gitconfig" off
         5 "show gitconfig" off)
choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
clear
for choice in $choices
do
    case $choice in
        1)
            git config --global user.name "Dirk Berger" && git config --global user.email "post@dirkberger.com"
            ;;
        2)
            git config --global user.name "Dirk" && git config --global user.email "dirk.berger@dirkberger.com"
            ;;
        3)
            git config --global user.name "Dirk Berger" && git config --global user.email "dirk.berger@sp-con.de"
            ;; 
        4)
            rm ~/.gitconfig
            ;; 
        5)
            git config --list
            ;;                                      
    esac
done